# Releases Infrastructure Status & Information Via Azure DevOps

-|Amazon|Goolge|Azure|OnPrem|
---|---|---|---|---|
Staging |[![](https://vsrm.dev.azure.com/DSO-IAC/_apis/public/Release/badge/124483b7-e41f-43ec-b957-cce8cd1b63d3/7/47)](3) | [![](4)](5) | [![](6)](7) | [![](9)](10) | [![11](12)](13) | [14](15)
Production|[![](https://vsrm.dev.azure.com/DSO-IAC/_apis/public/Release/badge/124483b7-e41f-43ec-b957-cce8cd1b63d3/7/48)](3) | [![](4)](5) | [![](6)](7) | [![](9)](10) | [![11](12)](13) | [14](15)

# Releases Infrastructure Status & Information Via gitlab

-|Amazon|Goolge|Azure|OnPrem|
---|---|---|---|---|
Staging |[![](https://gitlab.com/golanitai/dso-iac/badges/main/pipeline.svg)]() | [![](4)](5) | [![](6)](7) | [![](9)](10) | [![11](12)](13) | [14](15)

# MicroServices Deployed with ArgoCD

-|Amazon|Goolge|Azure|OnPrem|
---|---|---|---|---|
Simple App |[![](http://argocd.sbtest.link/api/badge?name=govweb&revision=true)]() | [![](4)](5) | [![](6)](7) | [![](9)](10) | [![11](12)](13) | [14](15)
Petstore |[![](http://argocd.sbtest.link/api/badge?name=petstore&revision=true)]() | [![](4)](5) | [![](6)](7) | [![](9)](10) | [![11](12)](13) | [14](15)

